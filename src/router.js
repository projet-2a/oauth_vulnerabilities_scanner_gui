import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: () => import('./views/Dashboard.vue')
        },
        {
            path: '/config',
            name: 'configuration',
            component: () => import('./views/Configuration.vue')
        },
        {
            path: '/vulns',
            name: 'vulnerabilities',
            component: () => import('./views/Vulnerabilities.vue')
        }
    ],
    linkExactActiveClass: "active"
})
