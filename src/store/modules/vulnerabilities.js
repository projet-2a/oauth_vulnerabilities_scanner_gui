const state = {
    vulnerabilities: []
}

const getters = {
    vulnerabilitiesCount: state => state.vulnerabilities.length
}

const mutations = {
    ADD_VULNERABILITY(state, vuln) {
        state.vulnerabilities.push(vuln)
    },
    SOCKET_result(state, result) {
        for (let vuln of JSON.parse(result)) {
            if (state.vulnerabilities.filter(v => v.id === vuln.id).length === 0){
                state.vulnerabilities.push(vuln)
            }
        }
    }
}

const actions = {
    addVulnerability(store, vuln) {
        store.commit('ADD_VULNERABILITY', vuln)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
}
