const state = {
    // Endpoints
    authorizationEndpoint: "https://api.cyberoauth.tk/v1/authorize",
    tokenEndpoint: "https://api.cyberoauth.tk/v1/token",
    introspectEndpoint: "https://api.cyberoauth.tk/v1/introspect",
    revokeEndpoint: "https://api.cyberoauth.tk/v1/revoke",
    // Grant types
    authorizationCodeGrant: true,
    implicitGrant: true,
    clientCredentialsGrant: true,
    resourceOwnerPasswordCredentialsGrant: true,
    // Clients
    clients: [
        {
            clientID: "d74e426a560d6eadbb1b",
            clientSecret: "9d0e456addfbbadb90217d45eb92f55a"
        }
    ],
}

const getters = {
    clientsCount: state => state.clients.length
}

const mutations = {
    // Endpoints
    SET_AUTHORIZATION_ENDPOINT(state, endpoint) {
        state.authorizationEndpoint = endpoint
    },
    SET_TOKEN_ENDPOINT(state, endpoint) {
        state.tokenEndpoint = endpoint
    },
    SET_INTROSPECT_ENDPOINT(state, endpoint) {
        state.introspectEndpoint = endpoint
    },
    SET_REVOKE_ENDPOINT(state, endpoint) {
        state.revokeEndpoint = endpoint
    },
    // Grant types
    SET_AUTHORIZATION_CODE_GRANT(state, b) {
        state.authorizationCodeGrant = b
    },
    SET_IMPLICIT_GRANT(state, b) {
        state.implicitGrant = b
    },
    SET_CLIENT_CREDENTIALS_GRANT(state, b) {
        state.clientCredentialsGrant = b
    },
    SET_RESOURCE_OWNER_PASSWORD_CREDENTIALS_GRANT(state, b) {
        state.resourceOwnerPasswordCredentialsGrant = b
    },
    // Clients
    ADD_CLIENT(state, client) {
        state.clients.push(client)
    },
    DELETE_CLIENT(state, client) {
        state.clients = state.clients.filter(c => c !== client)
    },
    SET_CLIENT_ID(state, payload) {
        state.clients[payload.index].clientID = payload.clientID
    },
    SET_CLIENT_SECRET(state, payload) {
        state.clients[payload.index].clientSecret = payload.clientSecret
    }
}

const actions = {
    addClient(store, client) {
        store.commit('ADD_CLIENT', client)
    },
    addEmptyClient(store) {
        store.commit('ADD_CLIENT', {
            clientID: "",
            clientSecret: ""
        })
    },
    deleteClient(store, client) {
        store.commit('DELETE_CLIENT', client)
    },
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
