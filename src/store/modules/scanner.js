const state = {
    controls: [],
    progress: 0,
    error: 0,
    totalProgress: 3,
    startTime: 0,
    endTime: 0,
    connected: false,
}

const getters = {
    progressPercent: state => (state.totalProgress === 0 ? 0 : Math.floor((state.progress / state.totalProgress) * 100)),
    errorPercent: state => (state.totalProgress === 0 ? 0 : Math.floor((state.error / state.totalProgress) * 100)),
    secondLapse(state) {
        const diff = state.endTime - state.startTime
        return Math.floor(diff)
    },
    connected: state => state.connected,
    controlsCount: state => state.progress
}

const mutations = {
    SOCKET_connect(state) {
        state.connected = true
    },
    SOCKET_disconnect(state) {
        state.connected = false
    },
    SOCKET_controls_list(state, controls) {
        state.controls = controls
        state.totalProgress = controls.length
    },
    SOCKET_control_end(state) {
        state.progress += 1
    },
    SOCKET_control_error(state) {
        state.error += 1
    },
    SOCKET_result(state) {
        state.endTime = Date.now()
        state.done = true
    },
    SET_START_DATE(state) {
        state.startTime = Date.now()
        state.endTime = Date.now()
    },
    RESET_STATS(state) {
        state.totalProgress = 0
        state.progress = 0
        state.error = 0
    },
}

const actions = {
    startScan(store) {
        if (this._vm.$socket.connected) {
            store.commit('SET_START_DATE')
            store.commit('RESET_STATS')
            const data = JSON.stringify(store.rootState.scannerConfig)
            this._vm.$socket.emit('run', data)
        }
    },
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
}
