const state = {
    controls: []
}

const getters = {
    controlsCount: state => state.controls.length
}

const mutations = {

}

const actions = {

}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
