import Vue from 'vue'
import Vuex from 'vuex'
import config from './modules/config'
import scanner from './modules/scanner'
import scannerConfig from './modules/scannerConfig'
import vulnerabilities from './modules/vulnerabilities'
import controls from './modules/controls'
import logger from './modules/logger'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        config,
        scanner,
        scannerConfig,
        vulnerabilities,
        controls,
        logger
    }
})
