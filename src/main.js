import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueUi from '@vue/ui'
import '@vue/ui/dist/vue-ui.css'
import VueSocketIO from 'vue-socket.io'
import VueApexCharts from 'vue-apexcharts'

Vue.use(VueUi)

Vue.use(
    new VueSocketIO({
        debug: true,
        connection: 'http://127.0.0.1:5000',
        // connection: document.location.origin,
        vuex: {
            store,
            actionPrefix: 'SOCKET_',
            mutationPrefix: 'SOCKET_',
        },
    })
)

Vue.use(VueApexCharts)
Vue.component('apexchart', VueApexCharts)

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')
