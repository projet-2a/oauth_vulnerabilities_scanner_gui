# OAuth - Vulnerabilities scanner GUI

GUI for the [OAuth vulnerabilities scanner](https://gitlab.com/projet-2a/OAuth_vulnerabilities_scanner).

## Install

Download source.
```shell
git clone git@gitlab.com:projet-2a/oauth_vulnerabilities_scanner_gui.git
```

Install dependencies.
```shell
npm install
```

Run in development server.
```shell
npm run serve
```

## Build for production

```shell
npm run build
```